Very simple folder content monitor
=================================

Monitors `base` for changes to / new .css files every X seconds.  
If css files have changed or new css files are made, copy ALL (not only the changed ones, sorry)
into `./folder3`

Usage:
-----

`./andreas.sh X` where X is the number of seconds between each check. It uses md5sum so don't check
too often if you have a lot of files, it will not be very efficient.

The `custom` folder does NOTHING LEBOWSKI

You could consider changing the copy to an rsync to avoid overwriting non-changed files, but
if they are non-changed, it kinda shouldn't matter anyways that they are overwritten?


:D
